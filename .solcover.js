module.exports = {
    skipFiles: ['mocks', 'token/ERC20/utils/SafeERC20.sol'],
    providerOptions: {
        default_balance_ether: '10000000000000000000000000'
    }
};
